var demoHtml = $('.demo').html();

$(window).resize(function () {
    $('body').css('min-height', $(window).height() - 90);
    $('.demo').css('min-height', $(window).height() - 160);
});

$(document).ready(function () {
    //general

    $('body').css('min-height', $(window).height() - 90);
    $('.demo').css('min-height', $(window).height() - 160);

    $('#menu-toggle').click(function (e) {
        e.preventDefault();
        $('#wrapper').toggleClass('toggled');
    });

    $("[data-toggle='collapse']").click(function (e) {
        e.preventDefault();
        var toggleTarget = e.target.getAttribute('data-target');
        $(`${toggleTarget}`).toggleClass('show');
    });

    // script

    $(".demo,.demo .column").sortable({
        connectWith: '.column',
        helper: "clone",
        opacity: 0.35,
        handle: '.drag'
    });

    $(".bsGrid").draggable({
        connectToSortable: ".demo",
        helper: "clone",
        handle: ".drag",
        drag: function (e, ui) {
            ui.helper.width(400);
        },
        stop: function (e, ui) {
            $('.demo .column').sortable({
                opacity: 0.35,
                connectWith: '.column'
            });
        }
    });

    var createChild = function () {

    }

    $(".createCol").keyup(function (e) {
        var cols = $(this).val();
        var iscommas = cols.includes(",");
        var divNum = [];
        if (iscommas) {
            divNum = cols.split(",");
            let total = divNum.reduce((old, curr) => old + curr);
            if (total > 12) {
                return false;
            }
        }
        else {
            divNum.push(cols);
            let total = divNum.reduce((old, curr) => old + curr);
            if (total < 12) {
                divNum.push(`${12 - total}`);
            }
        }
        console.log(divNum);
        var parent = $(this).parent().next();//.closest(".preview");
        if (parent.has(".preview")) {
            if (parent.children().first().is("div.row")) {
                console.log(cols);
                let dv = document.createElement('div');
                $(dv).addClass('col-md-4 column');
                parent.children().first().append(dv)
            }
            else {

            }
            // console.log(parent.children().first().is("div.row"))
        }
    });



});